# Community Blog (CommBlog) Planning

[![Creative Commons Attribution-ShareAlike 4.0 International License](https://i.creativecommons.org/l/by-sa/4.0/88x31.png)](https://creativecommons.org/licenses/by-sa/4.0/)

Planning, scheduling, and general coordination for editors of the [Fedora Community Blog](https://communityblog.fedoraproject.org)


## Purpose

The CommBlog is the primary source for news and updates for and about the Fedora Project community that develops, supports, and promotes Fedora Linux.
This repository is program management tool for the editors of the Fedora Community Blog, or CommBlog.
We use this to create issues to plan ongoing work beyond our usual scheduling that primarily happens on Discourse.

You can find more useful links related to our workflow below:

* [Roundtable meeting notes](https://hackmd.io/@fedora-marketing-team/rJJXxxE66) — HackMD
* [Calendar of upcoming CommBlog posts](https://discussion.fedoraproject.org/t/upcoming-community-blog-commblog-posts/10890) — Fedora Discussion
* [Pending articles ready for editor review](https://discussion.fedoraproject.org/c/workflows/community-blog-review/79) — Fedora Discussion


## Get involved

The best place to reach the Community Blog editors is either on Fedora Discussion or on Fedora Chat (Matrix):

* [`#marketing-team` tag on Fedora Discussion](https://discussion.fedoraproject.org/tags/c/project/7/marketing-team)
* [`#marketing:fedoraproject.org` on Matrix](https://matrix.to/#/#marketing:fedoraproject.org)


## Legal

[![Creative Commons Attribution-ShareAlike 4.0 International License](https://i.creativecommons.org/l/by-sa/4.0/88x31.png)](https://creativecommons.org/licenses/by-sa/4.0/)

[Creative Commons Attribution-ShareAlike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/) unless otherwise stated.
